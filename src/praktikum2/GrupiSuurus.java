package praktikum2;

import lib.TextIO;

// Ctrl + Shift + F

public class GrupiSuurus {

	public static void main(String[] args) {

		System.out.println("Sisesta inimeste arv");
		int inimesteArv = TextIO.getlnInt();

		System.out.println("Sisesta grupi suurus arv");
		int grupiSuurus = TextIO.getlnInt();

		int gruppideArv = inimesteArv / grupiSuurus;
		System.out.println("Saab moodustada " + gruppideArv + " gruppi");

		int j22k = inimesteArv % grupiSuurus;
		System.out.println("Üle jääb " + j22k + " inimest");
	}

}
